package poopoodice.eyeoftwilight.effects;

import net.minecraft.potion.Effect;
import net.minecraft.potion.EffectType;

public class KiBarrierEffect extends Effect
{
    public KiBarrierEffect(int liquidColorIn)
    {
        super(EffectType.BENEFICIAL, liquidColorIn);
    }
}

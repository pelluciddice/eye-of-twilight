package poopoodice.eyeoftwilight.effects;

import net.minecraft.potion.Effect;
import net.minecraft.potion.EffectType;

public class SpiritRefugeEffect extends Effect
{
    public SpiritRefugeEffect(int liquidColorIn)
    {
        super(EffectType.BENEFICIAL, liquidColorIn);
    }
}

package poopoodice.eyeoftwilight;

import net.minecraft.entity.Entity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.entity.player.ServerPlayerEntity;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.nbt.INBT;
import net.minecraft.util.Direction;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.SoundCategory;
import net.minecraft.util.SoundEvents;
import net.minecraft.world.World;
import net.minecraft.world.server.ServerWorld;
import net.minecraftforge.common.capabilities.Capability;
import net.minecraftforge.common.capabilities.CapabilityInject;
import net.minecraftforge.common.capabilities.ICapabilitySerializable;
import net.minecraftforge.common.util.LazyOptional;
import net.minecraftforge.event.AttachCapabilitiesEvent;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.common.Mod;
import org.apache.commons.lang3.tuple.Pair;
import poopoodice.eyeoftwilight.entity.SpiritBladeEntity;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.util.UUID;

@Mod.EventBusSubscriber
public class PlayerCapability implements ICapabilitySerializable<CompoundNBT>, IPlayerCapability
{
    @CapabilityInject(IPlayerCapability.class)
    public static Capability<IPlayerCapability> PLAYER_CAPABILITY = null;
    private final LazyOptional<IPlayerCapability> lazyOptional = LazyOptional.of(PlayerCapability::new);

    private UUID spiritBlade = null;
    private int spiritBladeClient = -1;

    private int emp = 0;
    private int empHitCount = 0;
    private int empDuration = 0;
    private int empCD = 0;

    private int kiBarrierCD = 0;
    private float kiBarrierShield = 0;
    private int kiBarrierDuration = 0;

    private int spiritRefugeCD = 0;
    private int spiritRefugeDuration = 0;

    private int standUnitedCD = 0;
    private int standUnitedChannel = 0;
    private UUID stantUnitedTarget = null;

    @Override
    public void tick(PlayerEntity player)
    {
        if (empDuration > 0)
        {
            empDuration--;
            if (empDuration <= 0)
                updateEmp(0, 0);
        }
        if (empCD > 0)
            empCD--;
        if (kiBarrierCD > 0)
            kiBarrierCD--;
        if (kiBarrierDuration > 0)
        {
            kiBarrierDuration--;
            if (kiBarrierDuration <= 0)
            {
                kiBarrierShield = 0;
                CommonModEventBus.removeBarrier(player);
            }
        }
        if (spiritRefugeCD > 0)
            spiritRefugeCD--;
        if (spiritRefugeDuration > 0)
        {
            spiritRefugeDuration--;
            if (spiritRefugeDuration <= 0 && getSpiritBlade(player.world) != null && !player.world.isRemote())
            {
                getSpiritBlade(player.world).setSpiritRefugeActive(false);
                CommonModEventBus.applyKiBarrierIfPossible(player);
                CommonForgeEventBus.syncWorldCapWithClient((ServerPlayerEntity) player);
            }
        }
        if (standUnitedCD > 0)
            standUnitedCD--;
        if (standUnitedChannel > 0)
        {
            standUnitedChannel--;
            if (!player.world.isRemote())
            {
                boolean abort = false;
                if (stantUnitedTarget != null)
                {
                    PlayerEntity target = player.world.getPlayerByUuid(stantUnitedTarget);
                    if (target == null || !target.isAlive())
                        abort = true;
                    if (standUnitedChannel <= 0 && stantUnitedTarget != null)
                    {
                        if (target != null)
                            if (player.attemptTeleport(target.getPosX(), target.getPosY(), target.getPosZ(), false))
                                if (getSpiritBlade(player.world) != null)
                                    getSpiritBlade(player.world).setPositionAndUpdate(target.getPosX(), target.getPosY(), target.getPosZ());
                    }
                    if (abort)
                    {
                        standUnitedChannel = 0;
                        stantUnitedTarget = null;
                        CommonForgeEventBus.syncWorldCapWithClient((ServerPlayerEntity) player);
                    }
                }
            }
        }
    }

    @Nonnull
    @Override
    public <T> LazyOptional<T> getCapability(@Nonnull Capability<T> cap, @Nullable Direction side)
    {
        return cap == PLAYER_CAPABILITY ? lazyOptional.cast() : LazyOptional.empty();
    }

    public static IPlayerCapability getCap(PlayerEntity player)
    {
        return player.getCapability(PLAYER_CAPABILITY).orElseThrow(() -> new NullPointerException("getting capability"));
    }

    @Override
    public CompoundNBT serializeNBT()
    {
        return (CompoundNBT) PLAYER_CAPABILITY.getStorage().writeNBT(PLAYER_CAPABILITY, lazyOptional.orElseThrow(() -> new NullPointerException("An error has occur during writing Player Capability")), null);
    }

    @Override
    public void deserializeNBT(CompoundNBT nbt)
    {
        PLAYER_CAPABILITY.getStorage().readNBT(PLAYER_CAPABILITY, lazyOptional.orElseThrow(() -> new NullPointerException("An error has occur during reading Player Capability")), null, nbt);
    }

    private static final ResourceLocation PLAYER_CAP = new ResourceLocation("eyeoftwilight", "player_capability");

    @SubscribeEvent
    public static void attachEntityCapability(final AttachCapabilitiesEvent<Entity> event)
    {
        if(event.getObject() instanceof PlayerEntity)
            if (!event.getCapabilities().containsKey(PLAYER_CAP))
                event.addCapability(PLAYER_CAP, new PlayerCapability());
    }

    @Override
    public SpiritBladeEntity getSpiritBlade(World world)
    {
        Entity entity;
        if (world.isRemote())
        {
            if (spiritBladeClient == -1)
                return null;
            entity = world.getEntityByID(spiritBladeClient);
        }
        else
        {
            if (spiritBlade == null)
                return null;
            entity = ((ServerWorld) world).getEntityByUuid(spiritBlade);
        }
        return entity instanceof SpiritBladeEntity ? (SpiritBladeEntity) entity : null;
    }

    @Override
    public UUID getSpiritBlade()
    {
        return spiritBlade;
    }

    @Override
    public int getSpiritBladeClient()
    {
        return spiritBladeClient;
    }

    @Override
    public Pair<Integer, Integer> getEmp()
    {
        return Pair.of(emp, empHitCount);
    }

    @Override
    public int getEmpDuration()
    {
        return empDuration;
    }

    @Override
    public int getKiBarrierDuration()
    {
        return kiBarrierDuration;
    }

    @Override
    public int getSpiritRefugeDuration()
    {
        return spiritRefugeDuration;
    }

    @Override
    public int getStandUnitedChannel()
    {
        return standUnitedChannel;
    }

    @Override
    public int getEmpCD()
    {
        return empCD;
    }

    @Override
    public int getKiBarrierCD()
    {
        return kiBarrierCD;
    }

    @Override
    public int getSpiritRefugeCD()
    {
        return spiritRefugeCD;
    }

    @Override
    public int getStandUnitedCD()
    {
        return standUnitedCD;
    }

    @Override
    public float getKiBarrierShield()
    {
        return kiBarrierShield;
    }

    @Override
    public UUID getStandUnitedTarget()
    {
        return stantUnitedTarget;
    }

    @Override
    public void setSpiritBlade(Entity entity)
    {
        spiritBlade = entity.getUniqueID();
    }

    @Override
    public void setSpiritBlade(UUID id)
    {
        spiritBlade = id;
    }

    @Override
    public void setSpiritBladeClient(int id)
    {
        spiritBladeClient = id;
    }

    @Override
    public void updateEmp(int empLevel, int count)
    {
        if (count < 1)
        {
            emp = 0;
            empHitCount = 0;
        }
        else
        {
            emp = empLevel;
            empHitCount = count;
        }
    }

    @Override
    public void setEmpDuration(int duration)
    {
        empDuration = duration;
    }

    @Override
    public void setKiBarrierDuration(int duration)
    {
        kiBarrierDuration = duration;
    }

    @Override
    public void setSpiritRefugeDuration(int duration)
    {
        spiritRefugeDuration = Math.max(0, duration);
    }

    @Override
    public void setStandUnitedChannel(int duration)
    {
        standUnitedChannel = duration;
    }

    @Override
    public void setEmpCD(int cd)
    {
        empCD = cd;
    }

    @Override
    public void setKiBarrierCD(int cd)
    {
        kiBarrierCD = Math.max(cd, 0);
    }

    @Override
    public void setSpiritRefugeCD(int cd)
    {
        spiritRefugeCD = cd;
    }

    @Override
    public void setStandUnitedCD(int cd)
    {
        standUnitedCD = cd;
    }

    @Override
    public void changeKiBarrierShield(float amount)
    {
        kiBarrierShield += amount;
        if (kiBarrierShield < 0)
            kiBarrierShield = 0.0F;
    }

    @Override
    public void setStandUnitedTarget(UUID uuid)
    {
        stantUnitedTarget = uuid;
    }

    public static class PlayerCapabilityStorage implements Capability.IStorage<IPlayerCapability>
    {
        public PlayerCapabilityStorage(){}

        @Nullable
        @Override
        public INBT writeNBT(Capability<IPlayerCapability> capability, IPlayerCapability instance, Direction side)
        {
            CompoundNBT compound = new CompoundNBT();
            compound.putUniqueId("id", instance.getSpiritBlade());
            compound.putInt("emp", instance.getEmp().getLeft());
            compound.putInt("count", instance.getEmp().getRight());
            compound.putInt("empduration", instance.getEmpDuration());
            compound.putInt("empcd", instance.getEmpCD());

            compound.putInt("kicd", instance.getKiBarrierCD());
            compound.putFloat("shield", instance.getKiBarrierShield());
            compound.putInt("kiduration", instance.getKiBarrierDuration());

            compound.putInt("refugecd", instance.getSpiritRefugeCD());
            compound.putInt("refugeduration", instance.getSpiritRefugeDuration());

            compound.putInt("ultcd", instance.getStandUnitedCD());
            compound.putInt("ultchannel", instance.getStandUnitedChannel());
            if (instance.getStandUnitedTarget() != null)
                compound.putUniqueId("ulttarget", instance.getStandUnitedTarget());
            return compound;
        }

        @Override
        public void readNBT(Capability<IPlayerCapability> capability, IPlayerCapability instance, Direction side, INBT nbt)
        {
            CompoundNBT compound = (CompoundNBT) nbt;
            if (compound.contains("id"))
                instance.setSpiritBlade(compound.getUniqueId("id"));
            else
                instance.setSpiritBlade((UUID) null);
            instance.updateEmp(compound.getInt("emp"), compound.getInt("count"));
            instance.setEmpDuration(compound.getInt("empduration"));
            instance.setEmpCD(compound.getInt("empcd"));

            instance.setKiBarrierCD(compound.getInt("kicd"));
            instance.changeKiBarrierShield(compound.getFloat("shield"));
            instance.setKiBarrierDuration(compound.getInt("kiduration"));

            instance.setSpiritRefugeCD(compound.getInt("refugecd"));
            instance.setSpiritRefugeDuration(compound.getInt("refugeduration"));

            instance.setStandUnitedCD(compound.getInt("ultcd"));
            instance.setStandUnitedChannel(compound.getInt("ultchannel"));
            if (compound.contains("ulttarget"))
                instance.setStandUnitedTarget(compound.getUniqueId("ulttarget"));
        }
    }
}

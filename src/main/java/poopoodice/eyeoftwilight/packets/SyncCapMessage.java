package poopoodice.eyeoftwilight.packets;

import net.minecraft.client.Minecraft;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.network.PacketBuffer;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.api.distmarker.OnlyIn;
import net.minecraftforge.fml.DistExecutor;
import net.minecraftforge.fml.network.NetworkEvent;
import poopoodice.eyeoftwilight.IPlayerCapability;
import poopoodice.eyeoftwilight.PlayerCapability;
import poopoodice.eyeoftwilight.entity.SpiritBladeEntity;

import java.util.function.Supplier;

public class SyncCapMessage
{
    private final int spiritBladeClient;
    private final float kiBarrierShield;
    private final int kiBarrierDuration;
    private final int spiritRefugeCD;
    private final int spiritRefugeDuration;
    private final int standUnitedChannel;

    public SyncCapMessage(PlayerEntity player)
    {
        IPlayerCapability cap = PlayerCapability.getCap(player);
        int id = cap.getSpiritBladeClient();
        if (player.world.getEntityByID(id) == null)
        {
            SpiritBladeEntity blade = cap.getSpiritBlade(player.world);
            if (blade != null)
            {
                id = blade.getEntityId();
                cap.setSpiritBladeClient(id);
            }
        }
        spiritBladeClient = id;
        kiBarrierShield = cap.getKiBarrierShield();
        kiBarrierDuration = cap.getKiBarrierDuration();
        spiritRefugeCD = cap.getSpiritRefugeCD();
        spiritRefugeDuration = cap.getSpiritRefugeDuration();
        standUnitedChannel = cap.getStandUnitedChannel();
    }

    public SyncCapMessage(int spiritBladeClient, float kiBarrierShield, int kiBarrierDuration, int spiritRefugeCD, int spiritRefugeDuration, int standUnitedChannel)
    {
        this.spiritBladeClient = spiritBladeClient;
        this.kiBarrierShield = kiBarrierShield;
        this.kiBarrierDuration = kiBarrierDuration;
        this.spiritRefugeCD = spiritRefugeCD;
        this.spiritRefugeDuration = spiritRefugeDuration;
        this.standUnitedChannel = standUnitedChannel;
   }

    public static void encode(SyncCapMessage msg, PacketBuffer packetBuffer)
    {
        packetBuffer.writeInt(msg.spiritBladeClient)
                .writeFloat(msg.kiBarrierShield)
                .writeInt(msg.kiBarrierDuration)
                .writeInt(msg.spiritRefugeCD)
                .writeInt(msg.spiritRefugeDuration)
                .writeInt(msg.standUnitedChannel);
    }

    public static SyncCapMessage decode(PacketBuffer packetBuffer)
    {
        return new SyncCapMessage(packetBuffer.readInt(), packetBuffer.readFloat(), packetBuffer.readInt(), packetBuffer.readInt(), packetBuffer.readInt(), packetBuffer.readInt());
    }

    public static void handle(SyncCapMessage message, Supplier<NetworkEvent.Context> ctx)
    {
        ctx.get().enqueueWork(() ->
                DistExecutor.runWhenOn(Dist.CLIENT, () -> () -> run(message)));
        ctx.get().setPacketHandled(true);
    }

    @OnlyIn(Dist.CLIENT)
    public static void run(SyncCapMessage message)
    {
        PlayerEntity player = Minecraft.getInstance().player;
        if (player != null)
        {
            IPlayerCapability cap = PlayerCapability.getCap(player);
            cap.setSpiritBladeClient(message.spiritBladeClient);
            cap.changeKiBarrierShield(-cap.getKiBarrierShield());
            cap.changeKiBarrierShield(message.kiBarrierShield);
            cap.setKiBarrierDuration(message.kiBarrierDuration);
            cap.setSpiritRefugeCD(message.spiritRefugeCD);
            cap.setSpiritRefugeDuration(message.spiritRefugeDuration);
            if (message.spiritRefugeDuration <= 0 && cap.getSpiritBlade(player.world) != null)
                cap.getSpiritBlade(player.world).setSpiritRefugeActive(false);
            cap.setStandUnitedChannel(message.standUnitedChannel);
        }
    }
}

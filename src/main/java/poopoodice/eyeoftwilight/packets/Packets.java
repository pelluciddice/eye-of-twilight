package poopoodice.eyeoftwilight.packets;

import net.minecraft.util.ResourceLocation;
import net.minecraftforge.fml.network.NetworkRegistry;
import net.minecraftforge.fml.network.simple.SimpleChannel;

public class Packets
{
    private static final String PROTOCOL_VERSION = "1";
    public static final SimpleChannel INSTANCE = NetworkRegistry.newSimpleChannel(new ResourceLocation("eyeoftwilight", "main"),
            () -> PROTOCOL_VERSION, PROTOCOL_VERSION::equals, PROTOCOL_VERSION::equals
    );

    public static void registerAll()
    {
        int id = 0;
        INSTANCE.registerMessage(id++, SyncCapMessage.class, SyncCapMessage::encode, SyncCapMessage::decode, SyncCapMessage::handle);
        INSTANCE.registerMessage(id++, ActiveSpiritRefugeMessage.class, ActiveSpiritRefugeMessage::encode, ActiveSpiritRefugeMessage::decode, ActiveSpiritRefugeMessage::handle);
        INSTANCE.registerMessage(id++, UpdateEntityMessage.class, UpdateEntityMessage::encode, UpdateEntityMessage::decode, UpdateEntityMessage::handle);
    }
}

package poopoodice.eyeoftwilight.packets;

import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.entity.player.ServerPlayerEntity;
import net.minecraft.network.PacketBuffer;
import net.minecraftforge.fml.network.NetworkEvent;
import poopoodice.eyeoftwilight.CommonForgeEventBus;
import poopoodice.eyeoftwilight.IPlayerCapability;
import poopoodice.eyeoftwilight.PlayerCapability;

import java.util.function.Supplier;

public class ActiveSpiritRefugeMessage
{
    public ActiveSpiritRefugeMessage()
    {
    }

    public static void encode(ActiveSpiritRefugeMessage msg, PacketBuffer packetBuffer)
    {
    }

    public static ActiveSpiritRefugeMessage decode(PacketBuffer packetBuffer)
    {
        return new ActiveSpiritRefugeMessage();
    }

    public static void handle(ActiveSpiritRefugeMessage message, Supplier<NetworkEvent.Context> ctx)
    {
        ctx.get().enqueueWork(() -> {
            PlayerEntity player = ctx.get().getSender();
            if (player != null)
            {
                IPlayerCapability cap = PlayerCapability.getCap(player);
                if (cap.getSpiritRefugeCD() <= 0)
                {
                    cap.setSpiritRefugeCD(140);
                    cap.setSpiritRefugeDuration(35);
                    if (cap.getSpiritBlade(player.world) != null)
                        cap.getSpiritBlade(player.world).setSpiritRefugeActive(true);
                    CommonForgeEventBus.syncWorldCapWithClient((ServerPlayerEntity) player);
                }
            }
        });
        ctx.get().setPacketHandled(true);
    }
}

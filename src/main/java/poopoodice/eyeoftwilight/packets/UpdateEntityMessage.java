package poopoodice.eyeoftwilight.packets;

import net.minecraft.client.Minecraft;
import net.minecraft.entity.Entity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.network.PacketBuffer;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.api.distmarker.OnlyIn;
import net.minecraftforge.fml.DistExecutor;
import net.minecraftforge.fml.network.NetworkEvent;
import poopoodice.eyeoftwilight.entity.SpiritBladeEntity;

import java.util.function.Supplier;

public class UpdateEntityMessage
{
    private final int id;
    private final boolean isSpiritRefugeActive;
    private final boolean moveTowardsOwner;

    public UpdateEntityMessage(SpiritBladeEntity blade)
    {
        this(blade.getEntityId(), blade.isSpiritRefugeActive(), blade.isMoveTowardsOwner());
    }

    public UpdateEntityMessage(int id, boolean isSpiritRefugeActive, boolean moveTowardsOwner)
    {
        this.id = id;
        this.isSpiritRefugeActive = isSpiritRefugeActive;
        this.moveTowardsOwner = moveTowardsOwner;
    }

    public static void encode(UpdateEntityMessage msg, PacketBuffer packetBuffer)
    {
        packetBuffer.writeInt(msg.id).writeBoolean(msg.isSpiritRefugeActive).writeBoolean(msg.moveTowardsOwner);
    }

    public static UpdateEntityMessage decode(PacketBuffer packetBuffer)
    {
        return new UpdateEntityMessage(packetBuffer.readInt(), packetBuffer.readBoolean(), packetBuffer.readBoolean());
    }

    public static void handle(UpdateEntityMessage message, Supplier<NetworkEvent.Context> ctx)
    {
        ctx.get().enqueueWork(() ->
                DistExecutor.runWhenOn(Dist.CLIENT, () -> () -> run(message)));
        ctx.get().setPacketHandled(true);
    }

    @OnlyIn(Dist.CLIENT)
    public static void run(UpdateEntityMessage message)
    {
        PlayerEntity player = Minecraft.getInstance().player;
        if (player != null)
        {
            Entity entity = player.world.getEntityByID(message.id);
            if (entity instanceof SpiritBladeEntity)
            {
                SpiritBladeEntity blade = (SpiritBladeEntity) entity;
                blade.setSpiritRefugeActive(message.isSpiritRefugeActive);
                blade.setMoveTowardsOwner(message.moveTowardsOwner);
            }
        }
    }
}

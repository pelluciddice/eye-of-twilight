package poopoodice.eyeoftwilight;

import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.entity.player.ServerPlayerEntity;
import net.minecraft.world.World;
import net.minecraftforge.event.TickEvent;
import net.minecraftforge.event.entity.living.LivingEvent;
import net.minecraftforge.event.entity.living.LivingHurtEvent;
import net.minecraftforge.event.entity.player.PlayerEvent;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.network.NetworkDirection;
import poopoodice.eyeoftwilight.entity.SpiritBladeEntity;
import poopoodice.eyeoftwilight.packets.Packets;
import poopoodice.eyeoftwilight.packets.SyncCapMessage;

@Mod.EventBusSubscriber
public class CommonForgeEventBus
{
    @SubscribeEvent
    public static void onPlayerTick(TickEvent.PlayerTickEvent event)
    {
        PlayerEntity player = event.player;
        if (!player.isAlive() || event.phase == TickEvent.Phase.START)
            return;
        World world = player.world;
        IPlayerCapability cap = PlayerCapability.getCap(player);
        if (cap.getSpiritBlade(world) == null)
        {
            SpiritBladeEntity spiritBlade = new SpiritBladeEntity(world, player);
            spiritBlade.setPosition(player.getPosX(), player.getPosY(), player.getPosZ());
            cap.setSpiritBlade(spiritBlade);
            cap.setSpiritBladeClient(spiritBlade.getEntityId());
            if (!world.isRemote())
                world.addEntity(spiritBlade);
        }
        cap.tick(player);
    }

    @SubscribeEvent
    public static void onLivingUpdate(LivingEvent.LivingUpdateEvent event)
    {
        LivingEntity entity = event.getEntityLiving();
        if (entity instanceof PlayerEntity)
        {
            IPlayerCapability cap = PlayerCapability.getCap((PlayerEntity) entity);
            if (cap.getStandUnitedChannel() > 0)
                event.setCanceled(true);
        }
    }

    @SubscribeEvent
    public static void onPlayerHurt(LivingHurtEvent event)
    {
        LivingEntity entity = event.getEntityLiving();
        if (!entity.isAlive() || !(entity instanceof PlayerEntity) || entity.world.isRemote())
            return;
        if (entity.isPotionActive(CommonModEventBus.SPIRIT_REFUGE_EFFECT))
        {
            event.setCanceled(true);
            return;
        }
        PlayerEntity player = (PlayerEntity) entity;
        IPlayerCapability cap = PlayerCapability.getCap(player);
        float shield = cap.getKiBarrierShield();
        if (event.getAmount() > shield)
        {
            cap.changeKiBarrierShield(-shield);
            event.setAmount(event.getAmount() - shield);
        }
        else if (event.getAmount() <= shield)
        {
            cap.changeKiBarrierShield(-event.getAmount());
            event.setAmount(0.0F);
        }
        CommonForgeEventBus.syncWorldCapWithClient((ServerPlayerEntity) player);
    }

    @SubscribeEvent
    public static void onPlayerLogin(PlayerEvent.PlayerLoggedInEvent event)
    {
        if (event.getEntity() instanceof ServerPlayerEntity && !event.getEntity().getEntityWorld().isRemote())
            syncWorldCapWithClient((ServerPlayerEntity) event.getPlayer());
    }

    @SubscribeEvent
    public static void onPlayerChangedDimension(PlayerEvent.PlayerChangedDimensionEvent event)
    {
        if (event.getEntity() instanceof ServerPlayerEntity && !event.getEntity().getEntityWorld().isRemote())
            syncWorldCapWithClient((ServerPlayerEntity) event.getPlayer());
    }

    @SubscribeEvent
    public static void onPlayerRespawn(PlayerEvent.PlayerRespawnEvent event)
    {
        if (event.getEntity() instanceof ServerPlayerEntity && !event.getEntity().getEntityWorld().isRemote())
            syncWorldCapWithClient((ServerPlayerEntity) event.getPlayer());
    }

    @SubscribeEvent
    public static void onPlayerClone(PlayerEvent.Clone event)
    {
        IPlayerCapability oldCap = PlayerCapability.getCap(event.getOriginal());
        IPlayerCapability newCap = PlayerCapability.getCap(event.getPlayer());

        newCap.setSpiritBlade(oldCap.getSpiritBlade());
        newCap.setSpiritBladeClient(oldCap.getSpiritBladeClient());

        newCap.updateEmp(oldCap.getEmp().getLeft(), oldCap.getEmp().getRight());
        newCap.setEmpDuration(oldCap.getEmpDuration());
        newCap.setEmpCD(oldCap.getEmpCD());

        newCap.setKiBarrierCD(oldCap.getKiBarrierCD());
        newCap.changeKiBarrierShield(oldCap.getKiBarrierShield());
        newCap.setKiBarrierDuration(oldCap.getKiBarrierDuration());

        newCap.setSpiritRefugeCD(oldCap.getSpiritRefugeCD());
        newCap.setSpiritRefugeDuration(oldCap.getSpiritRefugeDuration());
    }

    public static void syncWorldCapWithClient(ServerPlayerEntity player)
    {
        IPlayerCapability cap = PlayerCapability.getCap(player);
        SpiritBladeEntity blade = cap.getSpiritBlade(player.world);
        if (blade == null)
            return;
        Packets.INSTANCE.sendTo(new SyncCapMessage(player), player.connection.getNetworkManager(), NetworkDirection.PLAY_TO_CLIENT);
    }
}

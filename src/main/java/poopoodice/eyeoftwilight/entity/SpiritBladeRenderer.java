package poopoodice.eyeoftwilight.entity;

import com.mojang.blaze3d.matrix.MatrixStack;
import com.mojang.blaze3d.systems.RenderSystem;
import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.BufferBuilder;
import net.minecraft.client.renderer.IRenderTypeBuffer;
import net.minecraft.client.renderer.Tessellator;
import net.minecraft.client.renderer.entity.EntityRenderer;
import net.minecraft.client.renderer.entity.EntityRendererManager;
import net.minecraft.client.renderer.model.ItemCameraTransforms;
import net.minecraft.client.renderer.texture.OverlayTexture;
import net.minecraft.client.renderer.vertex.DefaultVertexFormats;
import net.minecraft.entity.Entity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.ItemStack;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.math.vector.Matrix4f;
import net.minecraft.util.math.vector.Vector3f;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.api.distmarker.OnlyIn;
import poopoodice.eyeoftwilight.CommonModEventBus;

@OnlyIn(Dist.CLIENT)
public class SpiritBladeRenderer extends EntityRenderer<SpiritBladeEntity>
{
    private static final ResourceLocation SPIRIT_REFUGE = new ResourceLocation("eyeoftwilight", "textures/entity/spirit_refuge.png");

    public SpiritBladeRenderer(EntityRendererManager renderManagerIn)
    {
        super(renderManagerIn);
    }

    @Override
    public void render(SpiritBladeEntity entityIn, float entityYaw, float partialTicks, MatrixStack matrixStackIn, IRenderTypeBuffer bufferIn, int packedLightIn)
    {
        if (entityIn.getOwner() != null)
        {
            PlayerEntity player = Minecraft.getInstance().player;
            if (player != null && entityIn.getOwner().getEntityId() == player.getEntityId())
                entityIn.setGlowing(true);
        }
        matrixStackIn.push();
        matrixStackIn.scale(1.35F, 1.35F, 1.35F);
        matrixStackIn.translate(0.0F, 0.925F, 0.0F);
        int tick = (int) (entityIn.world.getGameTime() % 72L);
        float amount;
        if (tick < 35)
            amount = -0.225F / 35.0F * tick;
        else if (tick < 36)
            amount = -0.225F;
        else if (tick < 71)
            amount = (float) (-0.225F + 0.225 / 35.0F * (tick - 36));
        else amount = 0;
        matrixStackIn.translate(0.0F, amount, 0.0F);
        Minecraft.getInstance().getItemRenderer().renderItem(new ItemStack(CommonModEventBus.SPIRIT_BLADE_ITEM), ItemCameraTransforms.TransformType.HEAD, (int) (packedLightIn * 1.75F), OverlayTexture.NO_OVERLAY, matrixStackIn, bufferIn);
        matrixStackIn.pop();
        if (entityIn.isSpiritRefugeActive())
        {
            Minecraft.getInstance().getTextureManager().bindTexture(SPIRIT_REFUGE);
            Tessellator tessellator = Tessellator.getInstance();
            BufferBuilder bufferbuilder = tessellator.getBuffer();
            bufferbuilder.begin(7, DefaultVertexFormats.POSITION_TEX);
            matrixStackIn.push();
            matrixStackIn.scale(7.5F, 7.5F, 7.5F);
            matrixStackIn.rotate(Vector3f.XP.rotationDegrees(90.0F));
            matrixStackIn.translate(0.0F, 0.0F, -0.025F);
            Matrix4f stack4f = matrixStackIn.getLast().getMatrix();
            bufferbuilder.pos(stack4f, -0.5F, +0.5F, 0).tex(0.0F, 1.0F).endVertex();
            bufferbuilder.pos(stack4f, +0.5F, +0.5F, 0).tex(1.0F, 1.0F).endVertex();
            bufferbuilder.pos(stack4f, +0.5F, -0.5F, 0).tex(1.0F, 0.0F).endVertex();
            bufferbuilder.pos(stack4f, -0.5F, -0.5F, 0).tex(0.0F, 0.0F).endVertex();
            RenderSystem.enableBlend();
            tessellator.draw();
            matrixStackIn.pop();
        }
    }

    @Override
    public ResourceLocation getEntityTexture(SpiritBladeEntity entity)
    {
        return null;
    }
}

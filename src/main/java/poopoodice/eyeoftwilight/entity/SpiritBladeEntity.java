package poopoodice.eyeoftwilight.entity;

import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityType;
import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.entity.player.ServerPlayerEntity;
import net.minecraft.entity.projectile.ProjectileHelper;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.network.IPacket;
import net.minecraft.network.PacketBuffer;
import net.minecraft.potion.EffectInstance;
import net.minecraft.potion.Effects;
import net.minecraft.util.DamageSource;
import net.minecraft.util.SoundCategory;
import net.minecraft.util.SoundEvents;
import net.minecraft.util.math.AxisAlignedBB;
import net.minecraft.util.math.EntityRayTraceResult;
import net.minecraft.util.math.vector.Vector3d;
import net.minecraft.world.World;
import net.minecraft.world.server.ServerWorld;
import net.minecraftforge.fml.common.registry.IEntityAdditionalSpawnData;
import net.minecraftforge.fml.network.NetworkHooks;
import net.minecraftforge.fml.network.PacketDistributor;
import poopoodice.eyeoftwilight.CommonForgeEventBus;
import poopoodice.eyeoftwilight.CommonModEventBus;
import poopoodice.eyeoftwilight.IPlayerCapability;
import poopoodice.eyeoftwilight.PlayerCapability;
import poopoodice.eyeoftwilight.packets.Packets;
import poopoodice.eyeoftwilight.packets.UpdateEntityMessage;

import javax.annotation.Nullable;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

public class SpiritBladeEntity extends Entity implements IEntityAdditionalSpawnData
{
    private int ownerID;
    private UUID ownerUUID;
    private boolean moveTowardsOwner;
    public boolean hasHitEntity;
    private boolean isSpiritRefugeActive;

    public SpiritBladeEntity(EntityType<?> entityTypeIn, World worldIn)
    {
        super(entityTypeIn, worldIn);
    }

    public SpiritBladeEntity(World worldIn, PlayerEntity player)
    {
        this(CommonModEventBus.SPIRIT_BLADE_ENTITY, worldIn);
        ownerUUID = player.getUniqueID();
        ownerID = player.getEntityId();
        setPosition(player.getPosX(), player.getPosY(), player.getPosZ());
    }

    @Override
    public void tick()
    {
        super.tick();
        if (moveTowardsOwner)
        {
            if (world.getGameTime() % 2 == 0)
                world.playMovingSound(null, this, SoundEvents.BLOCK_PORTAL_AMBIENT, SoundCategory.PLAYERS, 0.75F, 1.5F);
            PlayerEntity player = getOwner();
            if (player != null)
            {
                AxisAlignedBB axisalignedbb = getBoundingBox().expand(getMotion());
                Vector3d from = getPositionVec().add(0.0F, getHeight() / 2.0F, 0.0F);
                Vector3d to = getPositionVec().add(new Vector3d(getMotion().getX(), getMotion().getY(), getMotion().getZ()));
                EntityRayTraceResult entityResult = ProjectileHelper.rayTraceEntities(world, this, from, to, axisalignedbb, (entity) -> true);
                List<LivingEntity> hitEntities = rayTraceEntities();
                hitEntities.forEach((hitEntity) -> hitEntity.addPotionEffect(new EffectInstance(Effects.SLOWNESS, 40, 1, false, false)));
                hasHitEntity = hasHitEntity || !hitEntities.isEmpty();
                if (entityResult != null || !world.getEntitiesInAABBexcluding(this, getBoundingBox().grow(1.0F), entity -> true).isEmpty())
                {
                    setPosition(player.getPosX(), player.getPosY() + player.getHeight() / 2.0F - getHeight() / 2.0F, player.getPosZ());
                    moveTowardsOwner = false;
                    int emp = hasHitEntity ? 2 : 1;
                    hasHitEntity = false;
                    IPlayerCapability cap = PlayerCapability.getCap(player);
                    cap.updateEmp(emp, 3);
                    cap.setEmpDuration(160);
                    if (!world.isRemote())
                    {
                        CommonForgeEventBus.syncWorldCapWithClient((ServerPlayerEntity) player);
                        CommonModEventBus.removeAndApplyEmp(player, emp, 3);
                        CommonModEventBus.applyKiBarrierIfPossible(player);
                    }
                }
                else
                {
                    setMotion(new Vector3d(player.getPosX() - getPosX(), (player.getPosY() + player.getHeight() / 2.0F) - (getPosY() + getHeight() / 2.0F), player.getPosZ() - getPosZ()).normalize().scale(1.5F));
                    move();
                }
            }
            else if (!world.isRemote()) remove();
        }
        else
            setMotion(0, 0, 0);
        if (isSpiritRefugeActive && !world.isRemote())
        {
            if (world.getGameTime() % 2 == 0)
                world.playMovingSound(null, this, SoundEvents.BLOCK_PORTAL_AMBIENT, SoundCategory.PLAYERS, 0.85F, 0.75F);
            world.getEntitiesWithinAABB(LivingEntity.class, getBoundingBox().grow(3.5F)).forEach((entity) -> {
                if (entity.isPotionActive(CommonModEventBus.SPIRIT_REFUGE_EFFECT))
                    entity.removePotionEffect(CommonModEventBus.SPIRIT_REFUGE_EFFECT);
                entity.addPotionEffect(new EffectInstance(CommonModEventBus.SPIRIT_REFUGE_EFFECT, 2, 0, false, true));
            });
        }
    }

    public boolean isMoveTowardsOwner()
    {
        return moveTowardsOwner;
    }

    public void setMoveTowardsOwner(boolean moveTowardsOwner)
    {
        this.moveTowardsOwner = moveTowardsOwner;
        sendUpdates();
    }

    public boolean isSpiritRefugeActive()
    {
        return isSpiritRefugeActive;
    }

    public void setSpiritRefugeActive(boolean spiritRefugeActive)
    {
        isSpiritRefugeActive = spiritRefugeActive;
        sendUpdates();
    }

    private void sendUpdates()
    {
        if (!world.isRemote())
            Packets.INSTANCE.send(PacketDistributor.ALL.noArg(), new UpdateEntityMessage(this));
    }

    @Override
    public boolean isImmuneToExplosions()
    {
        return true;
    }

    @Override
    public boolean attackEntityFrom(DamageSource source, float amount)
    {
        return false;
    }

    @Override
    public boolean canBeCollidedWith()
    {
        return false;
    }

    @Override
    public boolean canBePushed()
    {
        return false;
    }

    @Override
    public boolean canRenderOnFire()
    {
        return false;
    }

    private List<LivingEntity> rayTraceEntities()
    {
        List<LivingEntity> entities = new ArrayList<>();
        for(Entity entity : world.getEntitiesInAABBexcluding(this, getBoundingBox().expand(getMotion()).grow(1.0F), (hit) -> hit instanceof LivingEntity && hit != getOwner()))
        {
            AxisAlignedBB axisalignedbb = entity.getBoundingBox().grow(0.3F);
            Optional<Vector3d> optional = axisalignedbb.rayTrace(getPositionVec(), getPositionVec().add(new Vector3d(getMotion().getX(), getMotion().getY(), getMotion().getZ())));
            if (optional.isPresent())
                entities.add((LivingEntity) entity);
        }

        return entities;
    }

    protected void move()
    {
        Vector3d vector3d = getMotion();
        double x = getPosX() + vector3d.x;
        double y = getPosY() + vector3d.y;
        double z = getPosZ() + vector3d.z;
        setPosition(x, y, z);
    }

    @Nullable
    public PlayerEntity getOwner()
    {
        if (world == null)
            return null;
        Entity entity;
        if (world instanceof ServerWorld)
            entity = ((ServerWorld) world).getEntityByUuid(ownerUUID);
        else entity = world.getEntityByID(ownerID);
        return entity instanceof PlayerEntity ? (PlayerEntity) entity : null;
    }

    @Override
    protected void registerData()
    {

    }

    @Override
    protected void readAdditional(CompoundNBT compound)
    {
        if (ownerUUID != null)
            compound.putUniqueId("owner", ownerUUID);
        compound.putBoolean("hit", hasHitEntity);
        compound.putBoolean("refuge", isSpiritRefugeActive);
    }

    @Override
    protected void writeAdditional(CompoundNBT compound)
    {
        if (compound.contains("owner"))
            ownerUUID = compound.getUniqueId("owner");
        hasHitEntity = compound.getBoolean("hit");
        isSpiritRefugeActive = compound.getBoolean("refuge");
    }

    @Override
    public IPacket<?> createSpawnPacket()
    {
        return NetworkHooks.getEntitySpawningPacket(this);
    }

    @Override
    public void writeSpawnData(PacketBuffer buffer)
    {
        buffer.writeInt(ownerID);
    }

    @Override
    public void readSpawnData(PacketBuffer additionalData)
    {
        ownerID = additionalData.readInt();
    }
}

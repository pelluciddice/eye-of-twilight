package poopoodice.eyeoftwilight;

import net.minecraft.entity.EntityClassification;
import net.minecraft.entity.EntityType;
import net.minecraft.entity.ai.attributes.Attributes;
import net.minecraft.entity.ai.attributes.ModifiableAttributeInstance;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.entity.player.ServerPlayerEntity;
import net.minecraft.item.Item;
import net.minecraft.potion.Effect;
import net.minecraft.potion.EffectInstance;
import net.minecraftforge.common.capabilities.CapabilityManager;
import net.minecraftforge.event.RegistryEvent;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.event.lifecycle.FMLCommonSetupEvent;
import poopoodice.eyeoftwilight.effects.EmpEffect;
import poopoodice.eyeoftwilight.effects.KiBarrierEffect;
import poopoodice.eyeoftwilight.effects.SpiritRefugeEffect;
import poopoodice.eyeoftwilight.entity.SpiritBladeEntity;
import poopoodice.eyeoftwilight.items.SpiritBlade;
import poopoodice.eyeoftwilight.items.StandUnited;
import poopoodice.eyeoftwilight.packets.Packets;

@Mod.EventBusSubscriber(bus = Mod.EventBusSubscriber.Bus.MOD)
public class CommonModEventBus
{
    public static final EntityType<SpiritBladeEntity> SPIRIT_BLADE_ENTITY = (EntityType<SpiritBladeEntity>) EntityType.Builder.<SpiritBladeEntity>create(SpiritBladeEntity::new, EntityClassification.MISC)
            .size(0.2F, 1.4F)
            .build("spirit_blade")
            .setRegistryName("spirit_blade");

    public static Item SPIRIT_BLADE_ITEM;
    public static Item STAND_UNITED_ITEM;

    public static final Effect EMPOWERED_EFFECT = new EmpEffect(0xFF00DBFF).setRegistryName("empowered");
    public static final Effect ENHANCED_EMPOWERED_EFFECT = new EmpEffect(0xFFFFA3FF).setRegistryName("enhanced_empowered");
    public static final Effect KI_BARRIER_EFFECT = new KiBarrierEffect(0xFFFFFFC9).setRegistryName("ki_barrier");
    public static final Effect SPIRIT_REFUGE_EFFECT = new SpiritRefugeEffect(0xFFFFFFC9).setRegistryName("spirit_refuge");

    public static void removeAndApplyEmp(PlayerEntity player, int emp, int count)
    {
        if (player.world.isRemote())
            return;
        removeEmp(player);
        Effect effect;
        if (emp == 2)
            effect = ENHANCED_EMPOWERED_EFFECT;
        else effect = EMPOWERED_EFFECT;
        int amplifier = count - 1;
        if (amplifier < 0 || emp < 1)
            return;
        player.addPotionEffect(new EffectInstance(effect, 160, amplifier, false, true));
    }

    public static void removeEmp(PlayerEntity player)
    {
        if (player.world.isRemote())
            return;
        if (player.isPotionActive(EMPOWERED_EFFECT))
            player.removePotionEffect(EMPOWERED_EFFECT);
        if (player.isPotionActive(ENHANCED_EMPOWERED_EFFECT))
            player.removePotionEffect(ENHANCED_EMPOWERED_EFFECT);
    }

    public static void applyKiBarrierIfPossible(PlayerEntity player)
    {
        if (player.world.isRemote())
            return;
        IPlayerCapability cap = PlayerCapability.getCap(player);
        if (cap.getKiBarrierCD() <= 0)
        {
            ModifiableAttributeInstance health = player.getAttribute(Attributes.MAX_HEALTH);
            cap.changeKiBarrierShield(-cap.getKiBarrierShield());
            cap.changeKiBarrierShield((float) (2.0F + (health.getValue() - health.getBaseValue()) * 0.14F));
            cap.setKiBarrierDuration(50);
            cap.setKiBarrierCD(200);
            removeBarrier(player);
            player.addPotionEffect(new EffectInstance(KI_BARRIER_EFFECT, 50, 0, false, true));
            CommonForgeEventBus.syncWorldCapWithClient((ServerPlayerEntity) player);
        }
    }

    public static void removeBarrier(PlayerEntity player)
    {
        if (player.world.isRemote())
            return;
        if (player.isPotionActive(KI_BARRIER_EFFECT))
            player.removePotionEffect(KI_BARRIER_EFFECT);
    }

    @SubscribeEvent
    public static void onEffectsRegister(RegistryEvent.Register<Effect> event)
    {
        event.getRegistry().registerAll(
                EMPOWERED_EFFECT,
                ENHANCED_EMPOWERED_EFFECT,
                KI_BARRIER_EFFECT,
                SPIRIT_REFUGE_EFFECT
        );
    }

    @SubscribeEvent
    public static void onEntitiesRegister(RegistryEvent.Register<EntityType<?>> event)
    {
        event.getRegistry().registerAll(
                SPIRIT_BLADE_ENTITY
        );
    }

    @SubscribeEvent
    public static void onItemsRegister(RegistryEvent.Register<Item> event)
    {
        event.getRegistry().registerAll(
                SPIRIT_BLADE_ITEM = new SpiritBlade().setRegistryName("spirit_blade"),
                STAND_UNITED_ITEM = new StandUnited().setRegistryName("stand_united")
        );
    }

    @SubscribeEvent
    public static void onSetup(final FMLCommonSetupEvent event)
    {
        CapabilityManager.INSTANCE.register(IPlayerCapability.class, new PlayerCapability.PlayerCapabilityStorage(), PlayerCapability::new);
        Packets.registerAll();
    }
}

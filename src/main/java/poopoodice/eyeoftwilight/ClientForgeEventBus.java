package poopoodice.eyeoftwilight;

import com.mojang.blaze3d.matrix.MatrixStack;
import com.mojang.blaze3d.systems.RenderSystem;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.AbstractGui;
import net.minecraft.client.renderer.BufferBuilder;
import net.minecraft.client.renderer.Tessellator;
import net.minecraft.client.renderer.vertex.DefaultVertexFormats;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.client.event.InputEvent;
import net.minecraftforge.client.event.RenderGameOverlayEvent;
import net.minecraftforge.client.gui.ForgeIngameGui;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.common.Mod;
import poopoodice.eyeoftwilight.packets.ActiveSpiritRefugeMessage;
import poopoodice.eyeoftwilight.packets.Packets;

@Mod.EventBusSubscriber(Dist.CLIENT)
public class ClientForgeEventBus
{
    private static final ResourceLocation KI_BARRIER_SHIELD_ICON = new ResourceLocation("eyeoftwilight:textures/overlay/ki_barrier_shield.png");
    @SubscribeEvent
    public static void onScreenRender(RenderGameOverlayEvent.Post event)
    {
        Minecraft minecraft = Minecraft.getInstance();
        PlayerEntity player = (PlayerEntity) minecraft.renderViewEntity;
        if (player != null && player.isAlive())
        {
            IPlayerCapability cap = PlayerCapability.getCap(player);
            float screenWidth = minecraft.getMainWindow().getScaledWidth();
            float screenHeight = minecraft.getMainWindow().getScaledHeight();
            if (event.getType() == RenderGameOverlayEvent.ElementType.HEALTH)
            {
                double shield = cap.getKiBarrierShield();
                shield = Math.round(shield);
                minecraft.getTextureManager().bindTexture(KI_BARRIER_SHIELD_ICON);
                Tessellator tessellator = Tessellator.getInstance();
                for (int i = 0; i < shield; i++)
                {
                    int x = (int) (screenWidth / 2.0F - 27);
                    int y = (int) (screenHeight - 49);
                    x += i * 8;
                    BufferBuilder bufferbuilder = tessellator.getBuffer();
                    bufferbuilder.begin(7, DefaultVertexFormats.POSITION_TEX);
                    bufferbuilder.pos(x, y + 9, 90.0D).tex(0.0F, 1.0F).endVertex();
                    bufferbuilder.pos(x + 9, y + 9, 90.0D).tex(1.0F, 1.0F).endVertex();
                    bufferbuilder.pos(x + 9, y, 90.0D).tex(1.0F, 0.0F).endVertex();
                    bufferbuilder.pos(x, y, 90.0D).tex(0.0F, 0.0F).endVertex();
                    tessellator.draw();
                }
                minecraft.getTextureManager().bindTexture(ForgeIngameGui.GUI_ICONS_LOCATION);
            }
            else if (event.getType() == RenderGameOverlayEvent.ElementType.ALL)
            {
                if (cap.getStandUnitedChannel() > 0)
                    AbstractGui.fill(event.getMatrixStack(), 0, 0, (int) screenWidth, (int) screenHeight, 0x66BE8DE7);
            }
        }
    }

    @SubscribeEvent
    public static void onMouseClick(InputEvent.MouseInputEvent event)
    {
        Minecraft minecraft = Minecraft.getInstance();
        PlayerEntity player = minecraft.player;
        if (player != null && player.isAlive() && event.getAction() == 1 && event.getButton() == 2 && minecraft.isGameFocused() && minecraft.currentScreen == null)
        {
            IPlayerCapability cap = PlayerCapability.getCap(player);
            Packets.INSTANCE.sendToServer(new ActiveSpiritRefugeMessage());
            if (cap.getSpiritRefugeCD() <= 0)
            {
                cap.setSpiritRefugeCD(140);
                cap.setSpiritRefugeDuration(35);
                if (cap.getSpiritBlade(player.world) != null)
                    cap.getSpiritBlade(player.world).setSpiritRefugeActive(true);
            }
        }
    }

    @SubscribeEvent
    public static void onBlockPick(InputEvent.ClickInputEvent event)
    {
        Minecraft minecraft = Minecraft.getInstance();
        PlayerEntity player = minecraft.player;
        if (player != null && player.isAlive() && event.isPickBlock() && player.getHeldItemMainhand().getItem() == CommonModEventBus.SPIRIT_BLADE_ITEM)
            event.setCanceled(true);
    }
}

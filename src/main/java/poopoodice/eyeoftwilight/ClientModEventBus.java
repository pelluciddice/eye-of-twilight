package poopoodice.eyeoftwilight;

import net.minecraft.item.ItemModelsProperties;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.client.registry.RenderingRegistry;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.event.lifecycle.FMLClientSetupEvent;
import poopoodice.eyeoftwilight.entity.SpiritBladeRenderer;

@Mod.EventBusSubscriber(bus = Mod.EventBusSubscriber.Bus.MOD, value = Dist.CLIENT)
public class ClientModEventBus
{
    @SubscribeEvent
    public static void onSetup(FMLClientSetupEvent event)
    {
        RenderingRegistry.registerEntityRenderingHandler(CommonModEventBus.SPIRIT_BLADE_ENTITY, SpiritBladeRenderer::new);
        event.enqueueWork(() -> ItemModelsProperties.registerProperty(CommonModEventBus.SPIRIT_BLADE_ITEM, new ResourceLocation("eyeoftwilight", "emp"), (stack, world, entity) -> stack.getOrCreateTag().getInt("emp")));
    }
}

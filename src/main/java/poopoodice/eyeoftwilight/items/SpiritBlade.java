package poopoodice.eyeoftwilight.items;

import com.google.common.collect.ImmutableMultimap;
import com.google.common.collect.Multimap;
import net.minecraft.entity.Entity;
import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.ai.attributes.Attribute;
import net.minecraft.entity.ai.attributes.AttributeModifier;
import net.minecraft.entity.ai.attributes.Attributes;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.inventory.EquipmentSlotType;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.util.*;
import net.minecraft.world.World;
import net.minecraftforge.common.ForgeMod;
import org.apache.commons.lang3.tuple.Pair;
import poopoodice.eyeoftwilight.CommonModEventBus;
import poopoodice.eyeoftwilight.IPlayerCapability;
import poopoodice.eyeoftwilight.PlayerCapability;
import poopoodice.eyeoftwilight.entity.SpiritBladeEntity;

import java.util.UUID;

public class SpiritBlade extends Item
{
    private final ImmutableMultimap<Attribute, AttributeModifier> attributeModifiers;
    private ImmutableMultimap<Attribute, AttributeModifier> attributeModifiers_2 = null;
    protected static final UUID ATTACK_SPEED_MODIFIER_2 = UUID.fromString("05250f0d-e20a-4f7d-9f66-9e967f6d328d");
    protected static final UUID REACH_DSITANCE_MODIFIER = UUID.fromString("48c714e1-af60-4696-9578-089ae115a3ae");

    public SpiritBlade()
    {
        super(new Item.Properties().maxStackSize(1).group(Tabs.MAIN));

        ImmutableMultimap.Builder<Attribute, AttributeModifier> builder = ImmutableMultimap.builder();
        builder = ImmutableMultimap.builder();
        builder.put(Attributes.ATTACK_SPEED, new AttributeModifier(ATTACK_SPEED_MODIFIER, "Weapon modifier 2", -2.5F, AttributeModifier.Operation.ADDITION));
        attributeModifiers = builder.build();
    }

    @Override
    public boolean onLeftClickEntity(ItemStack stack, PlayerEntity player, Entity entity)
    {
        if (!(entity instanceof LivingEntity))
            return false;
        CompoundNBT compound = stack.getOrCreateTag();
        IPlayerCapability cap = PlayerCapability.getCap(player);
        Pair<Integer, Integer> emp = cap.getEmp();
        float damage = 6.0F + 40.0F / (2000.0F / 20.0F);
        float extra;
        switch (emp.getLeft())
        {
            case 0:
            default:
                return false;
            case 1:
                extra = 0.04F;
                break;
            case 2:
                extra = 0.07F;
                break;
        }
        extra *= ((LivingEntity) entity).getMaxHealth();
        if (!(entity instanceof PlayerEntity))
            extra *= 2;
        damage += extra;
        if (!player.world.isRemote())
        {
            entity.setInvulnerable(false);
            if (entity.attackEntityFrom(DamageSource.causePlayerDamage(player), damage))
            {
                player.world.playSound(null, player.getPosX(), player.getPosY(), player.getPosZ(), SoundEvents.BLOCK_ANVIL_LAND, SoundCategory.PLAYERS, 0.8F, emp.getLeft() == 2 ? 1.5F : 1.25F);
                cap.updateEmp(emp.getLeft(), emp.getRight() - 1);
                emp = cap.getEmp();
                compound.putInt("emp", emp.getLeft());
                compound.putInt("count", emp.getRight());
                CommonModEventBus.removeAndApplyEmp(player, emp.getLeft(), emp.getRight());
            }
        }
        cap.setKiBarrierCD(cap.getKiBarrierCD() - 150);
        return true;
    }

    @Override
    public ActionResult<ItemStack> onItemRightClick(World worldIn, PlayerEntity playerIn, Hand handIn)
    {
        IPlayerCapability cap = PlayerCapability.getCap(playerIn);
        if (cap.getEmpCD() <= 0)
        {
            SpiritBladeEntity blade = cap.getSpiritBlade(worldIn);
            if (blade != null)
                blade.setMoveTowardsOwner(true);
            cap.setEmpCD(60);
            if (!worldIn.isRemote())
                return ActionResult.resultConsume(playerIn.getHeldItemMainhand());
        }
        return ActionResult.resultPass(playerIn.getHeldItemMainhand());
    }

    @Override
    public void inventoryTick(ItemStack stack, World worldIn, Entity entityIn, int itemSlot, boolean isSelected)
    {
        if (!(entityIn instanceof PlayerEntity) || worldIn.isRemote())
            return;
        CompoundNBT compound = stack.getOrCreateTag();
        PlayerEntity player = (PlayerEntity) entityIn;
        IPlayerCapability cap = PlayerCapability.getCap(player);
        Pair<Integer, Integer> emp = cap.getEmp();
        if (compound.getInt("emp") != emp.getLeft())
            compound.putInt("emp", emp.getLeft());
        if (compound.getInt("count") != emp.getRight())
            compound.putInt("count", emp.getRight());
    }

    @Override
    public Multimap<Attribute, AttributeModifier> getAttributeModifiers(EquipmentSlotType slot, ItemStack stack)
    {
        CompoundNBT compound = stack.getOrCreateTag();
        if (slot == EquipmentSlotType.MAINHAND && compound.getInt("emp") == 2)
        {
            if (attributeModifiers_2 == null)
            {
                ImmutableMultimap.Builder<Attribute, AttributeModifier> builder = ImmutableMultimap.builder();
                builder.put(Attributes.ATTACK_SPEED, new AttributeModifier(ATTACK_SPEED_MODIFIER_2, "Weapon modifier", 1.5F, AttributeModifier.Operation.MULTIPLY_BASE));
                builder.put(ForgeMod.REACH_DISTANCE.get(), new AttributeModifier(REACH_DSITANCE_MODIFIER, "Weapon modifier", 1.5F, AttributeModifier.Operation.ADDITION));
                attributeModifiers_2 = builder.build();
            }
            return attributeModifiers_2;
        }
        return slot == EquipmentSlotType.MAINHAND ? attributeModifiers : super.getAttributeModifiers(slot, stack);
    }
}

package poopoodice.eyeoftwilight.items;

import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.entity.player.ServerPlayerEntity;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.potion.EffectInstance;
import net.minecraft.potion.Effects;
import net.minecraft.util.ActionResult;
import net.minecraft.util.Hand;
import net.minecraft.util.SoundCategory;
import net.minecraft.util.SoundEvents;
import net.minecraft.world.World;
import net.minecraft.world.server.ServerWorld;
import poopoodice.eyeoftwilight.CommonForgeEventBus;
import poopoodice.eyeoftwilight.CommonModEventBus;
import poopoodice.eyeoftwilight.IPlayerCapability;
import poopoodice.eyeoftwilight.PlayerCapability;

public class StandUnited extends Item
{
    public StandUnited()
    {
        super(new Item.Properties().maxStackSize(1).group(Tabs.MAIN));
    }

    @Override
    public ActionResult<ItemStack> onItemRightClick(World worldIn, PlayerEntity playerIn, Hand handIn)
    {
        if (playerIn.isAlive() && !worldIn.isRemote())
        {
            IPlayerCapability cap = PlayerCapability.getCap(playerIn);
            ServerPlayerEntity target = ((ServerWorld) worldIn).getServer().getPlayerList().getPlayerByUsername(playerIn.getHeldItemMainhand().getDisplayName().getString());
            if (target != null && !target.getUniqueID().equals(playerIn.getUniqueID()))
            {
                if (cap.getStandUnitedCD() <= 0)
                {
                    cap.setStandUnitedCD(1800);
                    cap.setStandUnitedChannel(60);
                    cap.setStandUnitedTarget(target.getUniqueID());
                    int level = 0;
                    float missingHealth = 1.0F - (playerIn.getMaxHealth() - playerIn.getHealth()) / playerIn.getMaxHealth();
                    if (missingHealth <= 0.2F)
                        level++;
                    if (missingHealth <= 0.4F)
                        level++;
                    if (missingHealth <= 0.6F)
                        level++;
                    target.addPotionEffect(new EffectInstance(Effects.ABSORPTION, 100, level, false, true));
                    CommonForgeEventBus.syncWorldCapWithClient((ServerPlayerEntity) playerIn);
                    CommonModEventBus.applyKiBarrierIfPossible(playerIn);
                    target.world.playSound(null, target.getPosX(), target.getPosY(), target.getPosZ(), SoundEvents.BLOCK_PORTAL_TRAVEL, SoundCategory.PLAYERS, 1.25F, 0.5F);
                }
            }
            return ActionResult.resultConsume(playerIn.getHeldItemMainhand());
        }
        return ActionResult.resultPass(playerIn.getHeldItemMainhand());
    }
}

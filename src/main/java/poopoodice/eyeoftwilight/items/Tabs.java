package poopoodice.eyeoftwilight.items;

import net.minecraft.item.ItemGroup;
import net.minecraft.item.ItemStack;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.api.distmarker.OnlyIn;
import poopoodice.eyeoftwilight.CommonModEventBus;

public class Tabs
{
    public static final ItemGroup MAIN = new ItemGroup("main")
    {
        @OnlyIn(Dist.CLIENT)
        public ItemStack createIcon()
        {
            return new ItemStack(CommonModEventBus.SPIRIT_BLADE_ITEM);
        }
    };
}

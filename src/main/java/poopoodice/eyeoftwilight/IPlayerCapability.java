package poopoodice.eyeoftwilight;

import net.minecraft.entity.Entity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.world.World;
import org.apache.commons.lang3.tuple.Pair;
import poopoodice.eyeoftwilight.entity.SpiritBladeEntity;

import java.util.UUID;

public interface IPlayerCapability
{
    SpiritBladeEntity getSpiritBlade(World world);

    UUID getSpiritBlade();

    int getSpiritBladeClient();

    Pair<Integer, Integer> getEmp();

    int getEmpDuration();
    int getKiBarrierDuration();
    int getSpiritRefugeDuration();
    int getStandUnitedChannel();

    int getEmpCD();
    int getKiBarrierCD();
    int getSpiritRefugeCD();
    int getStandUnitedCD();

    float getKiBarrierShield();

    UUID getStandUnitedTarget();

    void setSpiritBlade(Entity entity);

    void setSpiritBlade(UUID id);

    void setSpiritBladeClient(int id);

    void updateEmp(int empLevel, int count);

    void setEmpDuration(int duration);
    void setKiBarrierDuration(int duration);
    void setSpiritRefugeDuration(int duration);
    void setStandUnitedChannel(int duration);

    void setEmpCD(int cd);
    void setKiBarrierCD(int cd);
    void setSpiritRefugeCD(int cd);
    void setStandUnitedCD(int cd);

    void changeKiBarrierShield(float amount);

    void setStandUnitedTarget(UUID uuid);


    void tick(PlayerEntity player);
}
